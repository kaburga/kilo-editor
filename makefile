CC = gcc
CFLAGS = -Wall -Wextra -pedantic
OBJS = kilo.c
TARGET = kilo

all: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(TARGET)

clean:
	rm -v $(TARGET)
